from rest_framework import serializers
from .models import *

class UserSerializer(serializers.ModelSerializer):
    class Meta():
        model = CustomUser
        fields  = ('login','name','surname','city','is_patient','tel',)

class CitySerializer(serializers.ModelSerializer):
    class Meta():
        model = City
        fields = ('name',)

class DrugSerializer(serializers.ModelSerializer):
    class Meta():
        model = Drug
        fields = ('name',)

class DrugTakenSerializer(serializers.ModelSerializer):
    class Meta():
        model = DrugTaken
        fields = ('drug','amount',)
        

class UserRaportSerializer(serializers.ModelSerializer):
    class Meta():
        model = UserRaport
        fields = ('patient','description','date',)

    
    def to_representation(self, instance):
        ret = super().to_representation(instance)
        ret['drugTaken'] = {'name': instance.drugTaken.drug.name, 'amount' : instance.drugTaken.amount}
        return ret

    # def create(self, validated_data):
    #     _login = validated_data['login']

class DoctorToPatientsBindingsSerializer(serializers.ModelSerializer):
    class Meta():
        model = DoctorToPatientsBindings
        fields = ('id','doctor','patient',)
