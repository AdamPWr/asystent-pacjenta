from django.shortcuts import render
from .models import *
from .serializers import *
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token
from rest_framework.authtoken.serializers import AuthTokenSerializer
from rest_framework.views import APIView
from rest_framework import status
from rest_framework.permissions import AllowAny
import json

# Create your views here.

class AuthToken(ObtainAuthToken):

    serializer_class = AuthTokenSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data,
                                           context={'request': request})
        # serializer.is_valid(raise_exception=False)
        # print(serializer.validated_data)
        # user = serializer.validated_data['user']
        data = request.data
        user = CustomUser.objects.get(login = data['username'])
        token, created = Token.objects.get_or_create(user=user)
        return Response({
            'token': token.key,
            'login'   : user.login,
            'is_patient' : user.is_patient,
            'name' : user.name,
            'surname' : user.surname,
            'city' : user.city.name,
            'tel ' : user.tel,
            'password' : user.password
        })

class AllUsersView(viewsets.ModelViewSet):
    queryset = CustomUser.objects.all()
    serializer_class = UserSerializer

    
class AllCitiesView(viewsets.ModelViewSet):
    permission_classes = [AllowAny]
    queryset = City.objects.all()
    serializer_class = CitySerializer

class AllDrugsView(viewsets.ModelViewSet):
    queryset = Drug.objects.all()
    serializer_class = DrugSerializer   


class AllDrugsTakenView(viewsets.ModelViewSet):
    queryset = DrugTaken.objects.all()
    serializer_class = DrugTakenSerializer 

class AllUsersRaportsView(viewsets.ModelViewSet):
    queryset = UserRaport.objects.all()
    serializer_class = UserRaportSerializer 

class AllDoctorsToPatientsBindingsView(viewsets.ModelViewSet):
    queryset = DoctorToPatientsBindings.objects.all()
    serializer_class = DoctorToPatientsBindingsSerializer 

class GetAllDoctorsView(viewsets.ViewSet):


    def list(self, request, pk=None):
        queryset = CustomUser.objects.get_all_doctors()
        print(queryset)
        serializer = UserSerializer(queryset, many=True)
        return Response(serializer.data)

class AddUserView(APIView):
    permission_classes = [AllowAny]

    def post(self, request, format=None):

        data = request.data
        print(data)
        _login = data['login']
        _password = data['password']
        _name = data['name']
        _surname = data['surname']
        _is_patient = data['is_patient']
        _city = data['city']
        _tel = data['tel']

        print(data)


        try:
            _city = City.objects.get(name = data['city'])
        except City.DoesNotExist:
            return Response("This city doesnt exist in DB. Correct type error or add it to DB first", status.HTTP_400_BAD_REQUEST)

        user = CustomUser.objects.get_or_none(login = _login)

        if user is not None:
            return Response("User with the same login already exists",status = status.HTTP_400_BAD_REQUEST)

        user = CustomUser.objects.create_user(login=_login, password=_password, name = _name,surname=_surname, city=_city,is_patient=_is_patient, tel=_tel)
        #user.save(using=self._db)
        if user:
            return Response("User created succesfully",status = status.HTTP_201_CREATED)
        else:
            return Response("User creation failed",status = status.HTTP_500_INTERNAL_SERVER_ERROR)

class AddReport(APIView):

    def post(self, request, format=None):

        data = request.data.copy()
        _login = data['login']
        _drug_taken_dict = json.loads(data['drug_taken'])
        _description = data['description']
        _drug_name = _drug_taken_dict['name']
        _amount = _drug_taken_dict['amount']
        #print(_drug_taken_dict)
        # _date = data['date']
        print(_drug_name)

        patient = CustomUser.objects.get_or_none(login = _login)
        if patient is  None:
            return Response("Patient with this login doesn't exists",status = status.HTTP_400_BAD_REQUEST)
        drug = Drug.objects.get_or_none(name = _drug_name)
        if drug is  None:
            return Response("Drug with this name  doesn't exists",status = status.HTTP_400_BAD_REQUEST)

        drug_taken = DrugTaken.objects.create(_drug = drug, _amount = _amount)

        UserRaport.objects.create(patient,drug_taken,_description)
           
        return Response("Report added succesfully",status = status.HTTP_201_CREATED)
        
class DeleteUserView(APIView):

    def post(self, request, format=None):
        data = request.data.copy()
        _login = data['login']

        print(_login)
        CustomUser.objects.delete_user(_login)
        return Response("User deleted succesfully",status = status.HTTP_200_OK)

class GetMyPatientsView(viewsets.ViewSet):


    def list(self, request, pk=None):

        data = request.headers.copy()
        _login = data['login']
        print(_login)
        queryset =DoctorToPatientsBindings.objects.getPatients(_login)
        patients = DoctorToPatientsBindings.objects.getPatients(_login)
        serializer = UserSerializer(queryset, many=True)
        return Response(serializer.data, status = status.HTTP_200_OK)


class GetUserRaportView(viewsets.ViewSet):

    def list(self, request, pk=None):
        data = request.headers.copy()
        _login = data['login']

        queryset = UserRaport.objects.get_user_raports(_login)
        serializer = UserRaportSerializer(queryset, many=True)
        
        return Response(serializer.data, status = status.HTTP_200_OK)


class AddDoctor2PatientBinding(APIView):

    def post(self, request, format=None):
        data = request.data.copy()
        _patient = data['patient']
        _doctor = data['doctor']

        patient = CustomUser.objects.getUser(_patient)
        doctor = CustomUser.objects.getUser(_doctor)

        if patient is None or doctor is None:
            return Response("There is no patient or doctor with this login",status = status.HTTP_400_BAD_REQUEST)

        if not patient.is_patient:
            return Response("Passed patient is not registered as patient in the system",status = status.HTTP_400_BAD_REQUEST)
        if doctor.is_patient:
            return Response("Passed doctor is not registered as doctor in the system",status = status.HTTP_400_BAD_REQUEST)

        
        binding = DoctorToPatientsBindings.objects.addBinding(patient,doctor)

        return Response("Patient added succesfully",status = status.HTTP_201_CREATED)

