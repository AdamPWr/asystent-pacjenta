from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
from django.db import models
from django.db.models import ObjectDoesNotExist

class CustomUserManager(BaseUserManager):
    def create_user(self, login, password, name, surname,city,is_patient,tel):#,_name,_surname,_city):
        if not login:# or not _name or not _surname or not _city:
            raise ValueError("Users must have a login!")

        user = self.model(
            login=login
        )
        user.set_password(password)
        user.name = name
        user.surname=surname
        user.city=city
        user.is_patient = is_patient
        user.tel = tel
        user.save(using=self._db)
        return user

    def create_superuser(self, login, password):
        user = self.create_user(
            login=login,
            password=password,
            name = "admin",
            surname="admin",
            city=None,
            is_patient=False,
            tel="1234"
        )
        user.is_patient = False
        user.is_admin = True
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)
        return user

    def get_or_none(self, **kwargs):
        try:
            return self.get(**kwargs)
        except ObjectDoesNotExist:
            return None

    def get_all_doctors(self):
        return self.filter(is_patient=False)

    def delete_user(self, login):
        self.filter(login = login).delete()

    def getUser(self, login):
        try:
            return super().get(login = login)
        except self.model.DoesNotExist:
            return None

class CityManager(models.Manager):

    def create(self,_name):
        city = self.model(name=_name)
        city.save()

class DrugManager(models.Manager):

    def create(self,_name):
        drug = self.model(name = _name)

    def get_or_none(self, **kwargs):
        try:
            return self.get(**kwargs)
        except ObjectDoesNotExist:
            return None

class DrugTakenManager(models.Manager):

    def create(self, _drug, _amount):
        dt = self.model(drug  =_drug, amount = _amount)
        dt.save()
        return dt;


class UserReportManager(models.Manager):

    def create(self, patient, drugTaken, description):
        report = self.model(patient = patient,  drugTaken = drugTaken, description = description)
        #print(f'ID: {report.id}')
        report.save()
        return report

    def get_user_raports(self, login):
        raports = self.filter(patient__login = login)
        return raports

class DoctorToPatientBindingmanager(models.Manager):

    def getPatients(self,login):
        doc2pac = self.filter(doctor__login = login).select_related("patient")
        patients = []
        for binding in doc2pac:
            patients.append(binding.patient)
        return patients

    
    def addBinding(self,patient, doctor):

        binding = self.model(doctor = doctor, patient = patient)
        binding.save()
        return binding

       

    def all(self):
        return super().all()



