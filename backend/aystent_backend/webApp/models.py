from django.db import models
from django.contrib.auth.models import AbstractBaseUser
from django.core.validators import MaxValueValidator, MinValueValidator
from .managers import *


class City(models.Model):
    name = models.TextField(primary_key=True)

    objects = CityManager()

    def __str__(self):
        return self.name


class CustomUser(AbstractBaseUser):

    login = models.CharField(max_length = 20, primary_key = True)
    password = models.CharField(max_length = 20)
    name = models.CharField(max_length = 20)
    surname = models.CharField(max_length = 20)
    city = models.ForeignKey(City,on_delete=models.CASCADE, null = True)
    is_patient = models.BooleanField(default = True)
    tel = models.CharField(max_length = 20)


    # Required for the AbstractBaseUser class
    date_joined = models.DateTimeField(verbose_name='date joined', auto_now_add=True)
    last_login = models.DateTimeField(verbose_name ='last login', auto_now=True)
    is_admin = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)

    USERNAME_FIELD = 'login'
    REQUIRED_FIELDS = ['password']


    objects = CustomUserManager()

    def __str__(self):
        return self.login

    # Required for the AbstractBaseUser class
    def has_perm(self, perm, obj=None):
        return self.is_admin

    def has_module_perms(self, app_label):
        return True

class Drug(models.Model):
    name = models.CharField(primary_key = True, max_length = 20)

    def __str__(self):
        return self.name

    objects = DrugManager()

class DrugTaken(models.Model):
    drug = models.ForeignKey(Drug, on_delete=models.CASCADE)
    amount = models.IntegerField()

    objects = DrugTakenManager()

    def __str__(self):
        return self.drug.name



class UserRaport(models.Model):
    patient = models.ForeignKey(CustomUser,on_delete=models.CASCADE)
    drugTaken = models.ForeignKey(DrugTaken, on_delete=models.CASCADE)
    description = models.TextField()
    date = models.DateField(auto_now=True)

    objects = UserReportManager()

    def __str__(self):
        return self.patient.login + " " +  str(self.date)


class DoctorToPatientsBindings(models.Model):
    doctor = models.ForeignKey(CustomUser,on_delete=models.CASCADE, related_name='doctor')
    patient = models.ForeignKey(CustomUser,on_delete=models.CASCADE, related_name='patient')

    objects = DoctorToPatientBindingmanager()

    def __str__(self):
        return str(self.id)

    