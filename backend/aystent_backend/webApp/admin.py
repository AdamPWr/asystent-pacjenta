from django.contrib import admin
from .models import CustomUser, City, Drug, DrugTaken, DoctorToPatientsBindings, UserRaport

# Register your models here.

admin.site.register(CustomUser)
admin.site.register(City)
admin.site.register(Drug)
admin.site.register(DrugTaken)
admin.site.register(DoctorToPatientsBindings)
admin.site.register(UserRaport)