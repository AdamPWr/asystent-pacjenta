"""aystent_backend URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from rest_framework import routers
from webApp.views import *



router = routers.DefaultRouter()
router.register('all_users',AllUsersView)
router.register('all_cities',AllCitiesView)
router.register('all_drugs',AllDrugsView)
router.register('all_drugs_taken',AllDrugsTakenView)
router.register('all_user_raports',AllUsersRaportsView)
router.register('all_doctor_to_patients_bindings',AllDoctorsToPatientsBindingsView)
router.register('all_doctors',GetAllDoctorsView, basename = '/')
router.register('my_patients',GetMyPatientsView, basename = '/')
router.register('user_raports',GetUserRaportView, basename = '/')
#router.register('add_doctor_to_patient_binding',GetUserRaportView, basename = '/')
#router.register('addUser',AddUserView)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('',include(router.urls)),
    path('auth/',AuthToken.as_view(),name='/'),
    path('add_user/',AddUserView.as_view(),name='/'),
    path('add_report/',AddReport.as_view(),name='/'),
    path('delete_user/',DeleteUserView.as_view(),name='/'),
    path('add_doctor_to_patient_binding/',AddDoctor2PatientBinding.as_view(),name='/')
]
