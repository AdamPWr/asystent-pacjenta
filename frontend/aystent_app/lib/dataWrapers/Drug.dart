

class Drug
{
  Drug({this.name});
  String name;

  Drug.fromJson(Map<String, dynamic> json)
      : name = json['name'];

  Map<String, dynamic> toJson() =>
      {
        'name': name,
      };
}