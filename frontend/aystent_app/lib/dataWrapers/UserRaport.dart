

import 'package:aystentapp/dataWrapers/DrugTaken.dart';

import 'Drug.dart';

class UserRaport
{

  UserRaport({this.login, this.description, this.drugTaken});
  String login;
  String description;
  DrugTaken drugTaken;
  String date;

  //TODO pobieranie raportow moze być lipne

  UserRaport.fromJson(Map<String, dynamic> json)
      : login = json['login'],
  description = json['description'],
  drugTaken = DrugTaken(drug: Drug(name: json['drugTaken']['name']), amount: json['drugTaken']['amount'],),
        date = json['date'];

  Map<String, dynamic> toJson() =>
      {
        'login': login,
        'drug_taken' : "${drugTaken.toJson()}",
        'description' : description
      };
}
// ##########################################################################################
class UserRaportReceive
{

  UserRaportReceive({this.patient, this.description, this.drugTakenID, this.date});
  String patient;
  String description;
  int drugTakenID;
  String date;

  //TODO pobieranie raportow moze być lipne

  UserRaportReceive.fromJson(Map<String, dynamic> json)
      : patient = json['patient'],
        description = json['description'],
        drugTakenID = json['drug_taken'],
        date = json['date'];

  Map<String, dynamic> toJson() =>
      {
        'patient': patient,
        'drug_taken' : "$drugTakenID",
        'description' : description,
        'date' : date
      };
}