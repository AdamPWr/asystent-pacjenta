

import 'package:aystentapp/dataWrapers/cityData.dart';
import 'package:flutter/cupertino.dart';

class UserData extends ChangeNotifier
{
  UserData({this.username,this.password,this.token, this.tel, this.isPatient,this.city, this.name, this.surname});

  String token;
  String username;
  String password;
  String name;
  String surname;
  String tel;
  bool isPatient;
  CityData city;

  UserData.fromJson(Map<String, dynamic> json)
      : name = json['name'],
  surname = json['surname'],
  username = json['login'],
  tel = json['tel'],
  token = json['token'],
  password = json['password'],
  city = new CityData(json['city'] != null ? json['city'] : "Brak"),
  isPatient = json['is_patient']=='true' ? true:false;

  Map<String, dynamic> toJson() =>
      {
        'name': name,
        'surname':surname,
        'login' : username,
        'tel' : tel,
        'token' : token,
        'password' : password,
        'city' : city.name,
        'is_patient' : isPatient
      };
}