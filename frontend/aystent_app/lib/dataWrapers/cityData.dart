

class CityData
{
  String name;

  CityData(this.name);

  CityData.fromJson(Map<String, dynamic> json)
      : name = json['name'];

  Map<String, dynamic> toJson() =>
      {
        'name': name,
      };
}