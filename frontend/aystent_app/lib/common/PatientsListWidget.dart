import 'package:aystentapp/apiHelpers/apiHandler.dart';
import 'package:aystentapp/common/patientTile.dart';
import 'package:aystentapp/dataWrapers/userData.dart';
import 'package:aystentapp/screens/yourPatientsScreen.dart';
import 'package:flutter/material.dart';

import 'appBar.dart';

class PatientsListWidget extends StatefulWidget {
  PatientsListWidget({
    Key key,
    @required this.userData,
    @required this.patients

  }) : super(key: key);

  //final  YourPatientScreen widget;
  final UserData userData;
  List<UserData> patients;

  @override
  _PatientsListWidgetState createState() => _PatientsListWidgetState();
}

class _PatientsListWidgetState extends State<PatientsListWidget> {

  ApiHandler apiHandler = new ApiHandler();
  @override
  Widget build(BuildContext context) {


    return CustomScrollView(


      slivers: [
        MainAppBar(userData: widget.userData,), //TODO

        SliverList(
          delegate: SliverChildBuilderDelegate(
                  (BuildContext context, int idx) {
                return PatientTile(cityName: widget.patients[idx].city.name,surname: widget.patients[idx].surname, docName: widget.patients[idx].name,number: widget.patients[idx].tel);
              },

              childCount: widget.patients.length
          ),
        ),

        SliverFillRemaining(
          child: Container(child: FlatButton(
            color: Colors.white,
            onPressed: () async
            {
              print("KLik");
              widget.patients = await apiHandler.myPatients(widget.userData);
              setState(() {
              });
              //widget.apiHandler.allDoctors(widget.userData);
            },
            child: Text("Refresh"),
          )),
        )
      ],
    );
  }
}