import 'dart:async';

import 'package:aystentapp/apiHelpers/apiHandler.dart';
import 'package:aystentapp/dataWrapers/cityData.dart';
import 'package:aystentapp/dataWrapers/userData.dart';
import 'package:flutter/material.dart';
import 'package:aystentapp/decorations/Colors.dart';

class RegisterWidget extends StatefulWidget {

  AnimationController _animControler;
  StreamController<int> _streamController;
  bool is_patient = true;
  ApiHandler apiHandler = new ApiHandler();
  Future<List<CityData>> getCities;

  TextEditingController _loginControler = new TextEditingController();
  TextEditingController _passwordControler = new TextEditingController();
  TextEditingController _nameControler = new TextEditingController();
  TextEditingController _surnameControler = new TextEditingController();
  TextEditingController _telControler = new TextEditingController();


  RegisterWidget(this._animControler, this._streamController);

  @override
  _RegisterWidgetState createState() => _RegisterWidgetState();
}

class _RegisterWidgetState extends State<RegisterWidget> {


  CityData selectedCity;

  @override
  void initState() {
    print("init register widget");
    //widget.getCities =
    widget.getCities = widget.apiHandler.getAllCities();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {



    return Container(

      padding: EdgeInsets.symmetric(horizontal: 15),

      child: Column(


        children: [
          TextFormField(
            controller: widget._loginControler,
            //keyboardType: TextInputType.phone,
            maxLines: null,
            decoration: InputDecoration(
              labelText: "Login",
              fillColor: Colors.white,
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(25.0),
                borderSide: BorderSide(
                  color: Colors.blueAccent,
                ),
              ),
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(25.0),
                borderSide: BorderSide(
                  color: Colors.blue,
                  width: 2.0,
                ),
              ),
            ),
          ),
          SizedBox(height: 10,),
          TextFormField(
            controller: widget._passwordControler,
            //keyboardType: TextInputType.phone,
            maxLines: null,
            decoration: InputDecoration(
              labelText: "Password",
              fillColor: Colors.white,
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(25.0),
                borderSide: BorderSide(
                  color: Colors.blueAccent,
                ),
              ),
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(25.0),
                borderSide: BorderSide(
                  color: Colors.blue,
                  width: 2.0,
                ),
              ),
            ),
          ),
          SizedBox(height: 10,),
          TextFormField(
            controller: widget._nameControler,
            //keyboardType: TextInputType.phone,
            maxLines: null,
            decoration: InputDecoration(
              labelText: "Name",
              fillColor: Colors.white,
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(25.0),
                borderSide: BorderSide(
                  color: Colors.blueAccent,
                ),
              ),
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(25.0),
                borderSide: BorderSide(
                  color: Colors.blue,
                  width: 2.0,
                ),
              ),
            ),
          ),
          SizedBox(height: 10,),
          TextFormField(
            controller: widget._surnameControler,
            //keyboardType: TextInputType.phone,
            maxLines: null,
            decoration: InputDecoration(
              labelText: "Surname",
              fillColor: Colors.white,
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(25.0),
                borderSide: BorderSide(
                  color: Colors.blueAccent,
                ),
              ),
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(25.0),
                borderSide: BorderSide(
                  color: Colors.blue,
                  width: 2.0,
                ),
              ),
            ),
          ),
          SizedBox(height: 10,),
          TextFormField(
            controller: widget._telControler,
            keyboardType: TextInputType.number,
            maxLines: null,
            decoration: InputDecoration(
              labelText: "Tel. number",
              fillColor: Colors.white,
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(25.0),
                borderSide: BorderSide(
                  color: Colors.blueAccent,
                ),
              ),
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(25.0),
                borderSide: BorderSide(
                  color: Colors.blue,
                  width: 2.0,
                ),
              ),
            ),
          ),
          SizedBox(height: 10,),
          Row(

            children: [
              Text(
                "Are you a patient ?",
                style: TextStyle(
                  fontSize: 20
                ),
              ),
              Expanded( child: Container(),),
              Switch(
                activeColor: orangeColor,
                value: widget.is_patient,
                onChanged: (val)
                {
                  setState(() {
                    widget.is_patient = val;
                  });
                },
              )
            ],
          ),
          SizedBox(height: 10,),
          Row(

            children: [
              Text(
                "City",
                style: TextStyle(
                  fontSize: 20
                ),
              ),
              Expanded(child: Container(),),
              FutureBuilder(
                future: widget.getCities,
                builder: (BuildContext context, AsyncSnapshot snapshot){
                  print("Future builder");
                  print("conn state ${snapshot.connectionState}");
                  if(snapshot.hasData)
                    {
                      return  DropdownButton(

                        value: selectedCity,

                        items: snapshot.data.map<DropdownMenuItem<CityData>>((val){
                          return DropdownMenuItem<CityData>(value: val, child: Text(val.name),);
                        }).toList(),

                        onChanged: (val){
                          print("val = $val");
                          setState(() {
                            selectedCity = val;
                          });
                        },
                      );
                    }

                  else{
                    return DropdownButton(
                      value: "Wroclaw",

                      items: [
                        DropdownMenuItem(value: "Wroclaw", child: Text("Wroclaw"),)
                      ],
                    );
                  }
                },


              )
            ],
          ),
          SizedBox(height: MediaQuery.of(context).size.height *0.12,),
          Row(

            children: [
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(60.0)),
                    primary: Colors.blueGrey),
                child: Container(
                    width: 100,
                    height: 80,
                    alignment: Alignment.center,
                    decoration: BoxDecoration(shape: BoxShape.circle),
                    child: Text(
                      "Cancel",
                      style: TextStyle(
                        color: orangeColor,
                        letterSpacing: 5,
                        fontSize: 20
                      ),
                    )
                ),
                onPressed: () {
                  widget._animControler.reverse();
                  widget._streamController.add(0);
                },
              ),
              Expanded(child: Container(),),
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                    shape: CircleBorder(), primary: Colors.blueGrey),
                child: Container(
                    width: 80,
                    height: 80,
                    alignment: Alignment.center,
                    decoration: BoxDecoration(shape: BoxShape.circle),
                    child: Icon(Icons.arrow_right, size: 50, color:  orangeColor,)
                ),
                onPressed: () async{
                  //widget.apiHandler.getAllCities();
                  // TODO check if fileds are fullfilled before accessing it
                  var user = UserData(
                    username: widget._loginControler.text,
                    password: widget._passwordControler.text,
                    name: widget._nameControler.text,
                    surname: widget._surnameControler.text,
                    isPatient: widget.is_patient,
                    city: selectedCity,
                    tel: widget._telControler.text
                  );

                  bool result = await widget.apiHandler.addUser(user);

                  if(result)
                    {
                      if(widget.is_patient)
                      {
                        Navigator.pushReplacementNamed(context, '/doctorHome', arguments: user);
                      }
                      else
                        {
                          Navigator.pushReplacementNamed(context, '/patientHome', arguments: user);
                        }
                    }
                  else
                    {
                      SnackBar(content: Text("Registration failed"),);
                    }

                },
              )
            ],
          )
        ],
      ),
    );
  }
}
