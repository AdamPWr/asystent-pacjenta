import 'package:aystentapp/decorations/Colors.dart';
import 'package:aystentapp/decorations/textStyles.dart';
import 'package:flutter/material.dart';

class DoctorTile extends StatelessWidget {

  DoctorTile({this.surname,this.cityName,this.docName,this.number});

  String cityName="";
  String docName="";
  String surname="";
  String number="";

  @override
  Widget build(BuildContext context) {


    var size = MediaQuery.of(context).size;

    return Column(
      children: [
        SizedBox(height: 10,),
        Container(
          height: size.height*0.1,
          width: size.width *0.9,
          padding: EdgeInsets.all(5),
          decoration: BoxDecoration(color: greenBlueColor,
            borderRadius: BorderRadius.circular(10)
          ),
          child: Stack(

            children: [
              Positioned.fill(
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: Table(

                    children: [
                      TableRow(
                        children: [
                          Text("Name",style: patientTtyleileTextStyle),
                          Text(docName,style: patientTtyleileTextStyle),
                        ]
                      ),
                      TableRow(
                          children: [
                            Text("Surname",style: patientTtyleileTextStyle),
                            Text(surname,style: patientTtyleileTextStyle),
                          ]
                      ),
                      TableRow(
                        children: [
                          Text("City",style: patientTtyleileTextStyle),
                          Text(cityName,style: patientTtyleileTextStyle),
                        ]
                      )
                    ],
                  ),
                ),
              ),
              Positioned(
                right: 10,
                  top: 20,
                  child: GestureDetector(
                    onTap: (){
                      Navigator.pushNamed(context, '/sendSMS', arguments: number);
                    },
                      child: Icon(Icons.mail, color: Colors.blueAccent,size: 35,),
                  )
              )
            ],
          )
        ),
      ],
    );
  }
}
