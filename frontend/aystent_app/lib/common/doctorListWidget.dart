

import 'package:aystentapp/dataWrapers/userData.dart';
import 'package:aystentapp/screens/yourDoctorsScreens.dart';
import 'package:flutter/material.dart';

import 'appBar.dart';
import 'doctorTile.dart';

class DocListWidget extends StatelessWidget {
  const DocListWidget({
    Key key,
    @required this.userData,
    this.doctors

  }) : super(key: key);

  //final YourDoctorsScreen widget;
  final List<UserData> doctors;
  final UserData userData;

  @override
  Widget build(BuildContext context) {

//    if(widget.doctors[0].city.name != null)
//      {
//        print("nie null");
//      }
//    else
//      {
//        print("jest null :(");
//      }
    return CustomScrollView(


      slivers: [
        MainAppBar(userData: userData,),
//        SliverList(
//          delegate: SliverChildBuilderDelegate((ctx, idx){
//            print("idx = $idx");
//            return DoctorTile(cityName: widget.doctors[idx].city.name,surname: widget.doctors[idx].surname, docName: widget.doctors[idx].name);
//          }
//          ),
//        ),
        SliverList(
          delegate: SliverChildBuilderDelegate(
                  (BuildContext context, int idx) {
//                print("idx = $idx");
//                print("DocListWidget doctors len: ${doctors.length}");
                return DoctorTile(cityName: doctors[idx].city.name,surname: doctors[idx].surname, docName: doctors[idx].name,number: doctors[idx].tel);
              },

              childCount: doctors.length
          ),
        ),

        SliverFillRemaining(
          child: Container(child: FlatButton(
            color: Colors.blue,
            onPressed: ()
            {
              print("KLik");
              //widget.apiHandler.allDoctors(widget.userData);
            },
            child: Text("Klik"),
          )),
        )
      ],
    );
  }
}