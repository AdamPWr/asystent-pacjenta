import 'package:aystentapp/dataWrapers/userData.dart';
import 'package:aystentapp/decorations/Colors.dart';
import 'package:aystentapp/decorations/textStyles.dart';
import 'package:aystentapp/screens/yourDoctorsScreens.dart';
import 'package:flutter/material.dart';


class MainAppBar extends StatelessWidget {
  const MainAppBar({
    Key key,
    @required this.userData,

  }) : super(key: key);

  //final YourDoctorsScreen widget;
  final UserData userData;

  @override
  Widget build(BuildContext context) {
    return SliverAppBar(
      flexibleSpace: FlexibleSpaceBar(

        background: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,

          children: [
            Container(
              //color: Colors.blueGrey,
              alignment: Alignment(0,0),
              padding: EdgeInsets.symmetric(horizontal: 50),
              child: Table(

                children: [
                  TableRow(
                      children: [
                        Text("Name",style: slvappTextStyle,),
                        Text(userData.name, style: slvappTextStyle,)
                      ]
                  ),
                  TableRow(
                      children: [
                        Text("Surname",style: slvappTextStyle,),
                        Text(userData.surname, style: slvappTextStyle,)
                      ]
                  ),
                  TableRow(
                      children: [
                        Text("Login",style: slvappTextStyle,),
                        Text(userData.username, style: slvappTextStyle,)
                      ]
                  ),
                ],
              ),
            ),
          ],
        ),

      ),
      title: Text("You are logged as: ${userData.isPatient ? "Patient" : "Doctor"}",

        style: TextStyle(
            color: orangeColor,
            letterSpacing: 2
        ),
      ),
      floating: true,
      pinned: true,
      expandedHeight: 200,
      //actions: [Icon(Icons.add),Container(width: 100,color: Colors.redAccent,)],

      backgroundColor: greenBlueColor,
    );
  }
}