import 'package:aystentapp/decorations/Colors.dart';
import 'package:aystentapp/decorations/textStyles.dart';
import 'package:flutter/material.dart';

class UserRaportTile extends StatelessWidget {

  UserRaportTile({this.description,this.date,this.drug,this.amount});

  String description="";
  String date="";
  String drug="";
  int amount = 0;

  @override
  Widget build(BuildContext context) {


    var size = MediaQuery.of(context).size;

    print("LENGTH ${description.length}");


    return Column(
      children: [
        SizedBox(height: 10,),
        Container(
            height: description.length >20 ? size.height* 0.2 : size.height* 0.1,
            width: size.width *0.9,
            padding: EdgeInsets.all(5),
            decoration: BoxDecoration(color: greenBlueColor,
                borderRadius: BorderRadius.circular(10)
            ),
            child: Stack(

              children: [
                Positioned.fill(
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: Table(

                      children: [
                        TableRow(
                            children: [
                              Text("Drug",style: patientTtyleileTextStyle),
                              Text(drug,style: patientTtyleileTextStyle),
                            ]
                        ),
                        TableRow(
                            children: [
                              Text("Amount",style: patientTtyleileTextStyle),
                              Text(amount.toString(),style: patientTtyleileTextStyle),
                            ]
                        ),
                        TableRow(
                            children: [
                              Text("Date",style: patientTtyleileTextStyle),
                              Text(date,style: patientTtyleileTextStyle),
                            ]
                        ),
                        TableRow(
                            children: [
                              Text("Description",style: patientTtyleileTextStyle),
                              Text(description,style: patientTtyleileTextStyle),
                            ]
                        )
                      ],
                    ),
                  ),
                ),
                Positioned(
                    right: 10,
                    top: 20,
                    child: GestureDetector(
                      onTap: (){
                        Navigator.pushNamed(context, '/userRaports', arguments: context);
                      },
                      child: Icon(Icons.description, color: Colors.blueAccent,size: 35,),
                    )
                )
              ],
            )
        ),
      ],
    );
  }
}