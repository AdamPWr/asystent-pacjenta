

import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/animation.dart';
import 'dart:math';
import 'package:aystentapp/decorations/Colors.dart';

class Point {
  final double x;
  final double y;

  Point(this.x, this.y);
}

class BackgroundPainter extends CustomPainter
{

  BackgroundPainter({Animation<double> animation}): bluePaint = Paint()..color = Color.fromRGBO(0, 51, 50, 1)
  ..style = PaintingStyle.fill,
  grayPaint = Paint()..color = Colors.blueGrey
  ..style = PaintingStyle.fill,
  yellowPaint = Paint()..color = orangeColor
  ..style = PaintingStyle.fill,
        linePaint = Paint()
          ..color = Colors.orange[300]
          ..style = PaintingStyle.stroke
          ..strokeWidth = 4,
        liquid = CurvedAnimation(
          curve: Curves.elasticOut,
          reverseCurve: Curves.easeInBack,
          parent: animation,
        ),
        yellowanim = CurvedAnimation(
          parent: animation,
          curve: const Interval(
            0,
            0.7,
            curve: Interval(0, 0.8, curve: SpringCurve()),
          ),
          reverseCurve: Curves.linear,
        ),
        greyaanim = CurvedAnimation(
          parent: animation,
          curve: const Interval(
            0,
            0.8,
            curve: Interval(0, 0.9, curve: SpringCurve()),
          ),
          reverseCurve: Curves.easeInCirc,
        ),
        blueanim = CurvedAnimation(
          parent: animation,
          curve: const SpringCurve(),
          reverseCurve: Curves.easeInCirc,
        ),
  super(repaint: animation);
//
  final Paint bluePaint;
  final Paint grayPaint;
  final Paint yellowPaint;
  final Paint linePaint;

  final Animation<double> liquid;
  final Animation<double> blueanim;
  final Animation<double> greyaanim;
  final Animation<double> yellowanim;

  @override
  void paint(Canvas canvas, Size size) {
    paintBlue(size, canvas);

    paintGrey(size, canvas);

    paintOrange(size, canvas);
  }

  void paintBlue(Size size, Canvas canvas) {
    final path = Path();
    path.moveTo(size.width, size.height / 2);
    path.lineTo(size.width, 0);
    path.lineTo(0, 0);
    path.lineTo(
      0,
      lerpDouble(0, size.height, blueanim.value),
    );
    _addPointsToPath(path, [
      Point(
        lerpDouble(0, size.width / 3, blueanim.value),
        lerpDouble(0, size.height, blueanim.value),
      ),
      Point(
        lerpDouble(size.width / 2, size.width / 4 * 3, liquid.value),
        lerpDouble(size.height / 2, size.height / 4 * 3, liquid.value),
      ),
      Point(
        size.width,
        lerpDouble(size.height / 2, size.height * 3 / 4, liquid.value),
      ),
    ]);
    canvas.drawPath(path, bluePaint);
  }

  void paintGrey(Size size, Canvas canvas) {
    final path = Path();
    path.moveTo(size.width, 300);
    path.lineTo(size.width, 0);
    path.lineTo(0, 0);
    path.lineTo(
      0,
      lerpDouble(
        size.height / 4,
        size.height / 2,
        greyaanim.value,
      ),
    );
    _addPointsToPath(
      path,
      [
        Point(
          size.width / 4,
          lerpDouble(size.height / 2, size.height * 3 / 4, liquid.value),
        ),
        Point(
          size.width * 3 / 5,
          lerpDouble(size.height / 4, size.height / 2, liquid.value),
        ),
        Point(
          size.width * 4 / 5,
          lerpDouble(size.height / 6, size.height / 3, greyaanim.value),
        ),
        Point(
          size.width,
          lerpDouble(size.height / 5, size.height / 4, greyaanim.value),
        ),
      ],
    );

    canvas.drawPath(path, grayPaint);
  }

  void paintOrange(Size size, Canvas canvas) {
    if (yellowanim.value > 0) {
      final path = Path();

      path.moveTo(size.width * 3 / 4, 0);
      path.lineTo(0, 0);
      path.lineTo(
        0,
        lerpDouble(0, size.height / 12, yellowanim.value),
      );

      _addPointsToPath(path, [
        Point(
          size.width / 7,
          lerpDouble(0, size.height / 6, liquid.value),
        ),
        Point(
          size.width / 3,
          lerpDouble(0, size.height / 10, liquid.value),
        ),
        Point(
          size.width / 3 * 2,
          lerpDouble(0, size.height / 8, liquid.value),
        ),
        Point(
          size.width * 3 / 4,
          0,
        ),
      ]);

      canvas.drawPath(path, yellowPaint);
    }
  }



  void _addPointsToPath(Path path, List<Point> points)
  {

    if (points.length < 3) {
      throw UnsupportedError('Need three or more points to create a path.');
    }

    for (var i = 0; i < points.length - 2; i++) {
      final xc = (points[i].x + points[i + 1].x) / 2;
      final yc = (points[i].y + points[i + 1].y) / 2;
      path.quadraticBezierTo(points[i].x, points[i].y, xc, yc);
    }

    // connect the last two points
    path.quadraticBezierTo(
        points[points.length - 2].x,
        points[points.length - 2].y,
        points[points.length - 1].x,
        points[points.length - 1].y);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    // TODO: implement shouldRepaint
    return true;
  }

}

class SpringCurve extends Curve {
  const SpringCurve({
    this.a = 0.15,
    this.w = 19.4,
  });
  final double a;
  final double w;

  @override
  double transformInternal(double t) {
    return (-(pow(e, -t / a) * cos(t * w)) + 1).toDouble();
  }
}