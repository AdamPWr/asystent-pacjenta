import 'dart:async';

import 'package:aystentapp/apiHelpers/apiHandler.dart';
import 'package:aystentapp/dataWrapers/cityData.dart';
import 'package:aystentapp/dataWrapers/userData.dart';
import 'package:flutter/material.dart';
import 'package:aystentapp/decorations/Colors.dart';

class LoginWidget extends StatelessWidget {

  LoginWidget(this._loginControler,this._passwordControler,this._animControler,this._streamController);


  TextEditingController _loginControler = new TextEditingController();
  TextEditingController _passwordControler = new TextEditingController();
  AnimationController _animControler;
  StreamController<int> _streamController;
  ApiHandler handler = new ApiHandler();

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 15),
      child: Column(

        children: [
          TextFormField(
            controller: _loginControler,
            //keyboardType: TextInputType.phone,
            maxLines: null,
            decoration: InputDecoration(
              labelText: "Login",
              fillColor: Colors.white,
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(25.0),
                borderSide: BorderSide(
                  color: Colors.blueAccent,
                ),
              ),
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(25.0),
                borderSide: BorderSide(
                  color: Colors.blue,
                  width: 2.0,
                ),
              ),
            ),
          ),
          SizedBox(height: 10,),
          TextFormField(
            obscureText: true,
            controller: _passwordControler,
            //keyboardType: TextInputType.phone,
            decoration: InputDecoration(
              labelText: "Password",
              fillColor: Colors.white,
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(25.0),
                borderSide: BorderSide(
                  color: Colors.blueAccent,
                ),
              ),
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(25.0),
                borderSide: BorderSide(
                  color: Colors.blue,
                  width: 2.0,
                ),
              ),
            ),
          ),
          SizedBox(height: 15,),
          Row(

            children: [
              GestureDetector(
                onTap: (){
                  _animControler.forward();
                  _streamController.add(1);
                },
                child: Text(
                  "Register now"
                ),
              ),
              Expanded(
                child: Container(),
              ),
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                    shape: CircleBorder(), primary: greenBlueColor),
                child: Container(
                  width: 80,
                  height: 80,
                  alignment: Alignment.center,
                  decoration: BoxDecoration(shape: BoxShape.circle),
                  child: Icon(Icons.arrow_right, size: 50, color: orangeColor,)
                ),
                onPressed: () async{
//
                //UserData userData = await handler.login(_loginControler.text, _passwordControler.text); //TODO

                  var userData = new UserData(tel: "3545",name: "Marek",surname: "CHhoinka",city: CityData("Wroclaw"),isPatient: false,password: "marek",username: "marek", token: "ee4276afdc2f34763f13900358612e9f2205b14d");
                if(userData == null)
                  {
                    Scaffold.of(context).showSnackBar(SnackBar(
                      content: Text("Failed to log in. Provide correct credentials."),
                      elevation: 60,
                    )
                    );
                  }
                else if(userData.isPatient)
                  {
                    Navigator.pushReplacementNamed(context, '/patientHome',arguments: userData);
                  }
                else
                  {
                    Navigator.pushReplacementNamed(context, '/doctorHome',arguments: userData);
                  }


                },
              )
            ],
          )
        ],
      ),
    );
  }
}


