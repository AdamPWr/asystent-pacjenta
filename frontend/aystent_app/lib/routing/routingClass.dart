import 'package:aystentapp/dataWrapers/userData.dart';
import 'package:aystentapp/screens/addPatientScreen.dart';
import 'package:aystentapp/screens/doctorHome.dart';
import 'package:aystentapp/screens/patientHome.dart';
import 'package:aystentapp/screens/sendSmsScreen.dart';
import 'package:aystentapp/screens/userRaportsScreen.dart';
import 'package:flutter/material.dart';
import 'package:aystentapp/screens/loginScreen.dart';

class RouteGenerator {

  static Route<dynamic> generteRoute(RouteSettings settings){

    final args = settings.arguments;

    switch(settings.name){
      case '/login':
        return MaterialPageRoute(builder: (_)=>LoginScreen());
        break;
      case '/patientHome':
        {

          if(args is UserData)
          {
            return MaterialPageRoute(builder: (_)=>PatientHome(args));
          }

        }
        break;
      case '/doctorHome':
        {

          if(args is UserData)
          {
            return MaterialPageRoute(builder: (_)=>DoctorHome(args));
          }

        }
        break;
      case '/sendSMS':
        {

          if(args is String)
          {
            return MaterialPageRoute(builder: (_)=>SendSmsScreen(number: args,));
          }

        }
        break;
      case '/userRaports':
        {
          if(args is BuildContext) {
            return MaterialPageRoute(builder: (_)=>UserRaportsScreen(prevCtx: args,));
          }
        }
        break;
      case '/addPatient':
        {
          if(args is BuildContext) {
            return MaterialPageRoute(builder: (_)=>AddPatientscreen(precCtx: args,));
          }
        }
        break;

    }
  }

}