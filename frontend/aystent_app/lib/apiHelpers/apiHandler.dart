import 'dart:convert' as convert;
import 'dart:io';
import 'package:aystentapp/dataWrapers/Drug.dart';
import 'package:aystentapp/dataWrapers/UserRaport.dart';
import 'package:aystentapp/dataWrapers/cityData.dart';
import 'package:aystentapp/dataWrapers/userData.dart';
import 'package:http/http.dart' as http;



class ApiHandler
{

  final String loginURL = "http://localhost:8000/auth/";
  final String getAllCitiesURL = "http://localhost:8000/all_cities";
  final String addUserURL = "http://localhost:8000/add_user/";
  final String allDoctorsURL = "http://localhost:8000/all_doctors";
  final String allDrugsURL = "http://localhost:8000/all_drugs";
  final String addUserReportURL = "http://localhost:8000/add_report/";
  final String deleteUserURL = "http://localhost:8000/delete_user/";
  final String myPatientsURL = "http://localhost:8000/my_patients/";
  final String userRaportURL = "http://localhost:8000/user_raports/";
  final String addPatientBidingURL = "http://localhost:8000/add_doctor_to_patient_binding/";

  Future<UserData> login(String username, String password) async{

    var response = await http.post(loginURL, body: {'username':username, 'password': password});

    if(response.statusCode == 200)
      {
        print(response.body);

        var jsonResponse = convert.jsonDecode(response.body);
        print("surname ${jsonResponse['surname']}");
        return UserData(
            username: jsonResponse['login'],
            password: jsonResponse['password'],
            token: jsonResponse['token'],
          isPatient: jsonResponse['is_patient'],
          city: CityData(jsonResponse['city']),
          name: jsonResponse['name'],
          surname: jsonResponse['surname'],
          tel: jsonResponse['tel'],
    );

      }
    else{
      print("Login response code error ${response.statusCode}");
      return null;
    }
  }


  Future<List<CityData>> getAllCities() async
  {

    try{
      print("get all cities method1");
      var response = await http.get(getAllCitiesURL).timeout(Duration(seconds: 1), onTimeout: (){print("timeout"); return null;});
      print("checkpoint");
      if(response.statusCode == 200)
      {
        var jsonResponse = convert.jsonDecode(response.body);
        print(jsonResponse);
        List<CityData> list= [];

        for( var city in jsonResponse)
        {
          list.add(CityData.fromJson(city));
        }
        print("Pobrano liste miast ${list}");
        return list;
      }
      else
      {
        print("Register get all cities error ${response.statusCode}");
        return null;
      }
    } on SocketException
    {
      print("register: soket error");

      return [];
    }

  }

  Future<bool> addUser(UserData user) async{

    print("add user method");
    var response = await http.post(addUserURL,body: {'login': user.username,
      'password':user.password,
      'name':user.name,
      'surname': user.surname,
      'is_patient': user.isPatient ? "True" : "False",
      'tel':user.tel,
      'city':user.city.name});

    if(response.statusCode == 200 || response.statusCode ==201)
      {
        print("User added ");
        print(response.body);
        return true;
      }
    else{
      print("Adding user failed ${response.statusCode}");
      print(response.body);
      return false;
    }
  }


  Future<List<UserData>> allDoctors(UserData user) async{

    try{
      print("geting all doctors");
      print(allDoctorsURL);
      print("Token: ${user.token}");
      var response = await http.get(allDoctorsURL, headers: {'authorization' : 'Token ${user.token}'});
      List<UserData> doctors= [];
      print("checkpoint");
      if(response.statusCode == 200 || response.statusCode ==201)
      {

        print("getall doctor successed ");
        print(response.body);
        var jsonResponse = convert.jsonDecode(response.body);
        for( var doc in jsonResponse)
        {
          doctors.add(UserData.fromJson(doc));
        }
        print("Pobrano liste doktorów $doctors");
        return doctors;
      }
      else{
        print("getall doctor failed ${response.statusCode}");
        print(response.body);
        return doctors;
      }

    } on SocketException
    {
      print("register: soket error");

      return [];
    }
  }

  Future<List<Drug>> getAllDrugs(String token) async
  {

    try{
      print("geting all drugs");
      print(allDoctorsURL);
      print("Token: $token");
      var response = await http.get(allDrugsURL, headers: {'authorization' : 'Token $token'});
      List<Drug> drugs= [];
      print("checkpoint");
      if(response.statusCode == 200 || response.statusCode ==201)
      {

        print("getall doctor successed ");
        print(response.body);
        var jsonResponse = convert.jsonDecode(response.body);
        for( var doc in jsonResponse)
        {
          drugs.add(Drug.fromJson(doc));
        }
        print("Pobrano liste lekow $drugs");
        return drugs;
      }
      else{
        print("getall drugs failed ${response.statusCode}");
        print(response.body);
        return drugs;
      }

    } on SocketException
    {
      print("register: soket error");

      return [];
    }
  }

  Future<bool>addUserReport(UserRaport userRaport, String token) async{
    try{
      //var response = await http.post(addUserReportURL, headers: {'authorization' : 'Token ${token}'}, body: {'login' : 'adam'});
      print("token: $token");
      print("login: ${userRaport.login}");
      print("desc: ${userRaport.description}");
      print("amount: ${userRaport.drugTaken.amount} drugname: ${userRaport.drugTaken.drug.name}");

      var response = await http.post(addUserReportURL, headers: {'authorization' : 'Token $token'}, body: userRaport.toJson());
      if(response.statusCode == 200 || response.statusCode ==201)
        {
          return true;
        }
      else
        {
          print(response.body);
          return false;
        }

    } on SocketException
    {
      print("register: soket error");
      //TODO dodac snack bar w przypadku niepowodzenia

      return false;
    }
  }

  Future<bool>deleteUser(String login, String token) async{
    try{
      var response = await http.post(deleteUserURL, headers: {'authorization' : 'Token $token'}, body: {'login' : login});
      if(response.statusCode == 200 || response.statusCode ==201)
      {
        return true;
      }
      else
      {
        return false;
      }

    } on SocketException
    {
      print("register: soket error");
      //TODO dodac snack bar w przypadku niepowodzenia

      return false;
    }
  }

  Future<List<UserData>> myPatients(UserData user)async {

    try{
      print("geting all doctors");
      print(myPatientsURL);
      print("Token: ${user.token}");
      var response = await http.get(myPatientsURL, headers: {'authorization' : 'Token ${user.token}', 'login' : user.username});
      List<UserData> patients= [];
      print("checkpoint");
      if(response.statusCode == 200 || response.statusCode ==201)
      {

        print("get my patients successed ");
        print(response.body);
        var jsonResponse = convert.jsonDecode(response.body);
        for( var doc in jsonResponse)
        {
          patients.add(UserData.fromJson(doc));
        }
        print("Pobrano liste pacjentow $patients");
        return patients;
      }
      else{
        print("get my patients failed ${response.statusCode}");
        print(response.body);
        return patients;
      }

    } on SocketException
    {
      print("register: soket error");

      return [];
    }
  }


  Future<List<UserRaport>> getUserRaports(UserData user)async {

    try{
      print(userRaportURL);
      var response = await http.get(userRaportURL, headers: {'authorization' : 'Token ${user.token}', 'login' : 'adam'});
      List<UserRaport> received_reports= [];
      if(response.statusCode == 200 || response.statusCode ==201)
      {

        print("get my reports successed ");
        print(response.body);
        var jsonResponse = convert.jsonDecode(response.body);
        for( var rap in jsonResponse)
        {
          received_reports.add(UserRaport.fromJson(rap));
        }
        print("Pobrano liste raportow $received_reports");
        return received_reports;
      }
      else{
        print("get my raports failed ${response.statusCode}");
        print(response.body);
        return received_reports;
      }

    } on SocketException
    {
      print("register: soket error");

      return [];
    }
  }

  Future<bool> addPatientBinding(UserData user, String patient_login)async {

    try{
      var response = await http.post(addPatientBidingURL, headers: {'authorization' : 'Token ${user.token}'}, body: {'patient' : patient_login, 'doctor': user.username});
      if(response.statusCode == 200 || response.statusCode ==201)
      {
        print("biding added 201");
        return true;
      }
      else{
        print("biding added failed, code : ${response.statusCode}");
      }

    } on SocketException
    {
      print("register: soket error");

      return false;
    }
  }


}