import 'dart:async';

import 'package:aystentapp/common/BackgroundPainter.dart';
import 'package:aystentapp/common/RegisterWidget.dart';
import 'package:flutter/material.dart';
import 'package:aystentapp/common/loginWidget.dart';


class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> with SingleTickerProviderStateMixin{

  AnimationController _controler;
  TextEditingController _loginControler = new TextEditingController();
  TextEditingController _passwordControler = new TextEditingController();
  StreamController<int> _streamControler = new StreamController<int>();

  int login_screen_present = 0;

  @override
  void initState() {
    _controler = AnimationController(vsync: this, duration: Duration(seconds: 6));
    _passwordControler.text = "marek";
    _loginControler.text = "marek";
    super.initState();
  }

  @override
  void dispose() {
    _controler.dispose();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          SizedBox.expand(

            child: CustomPaint(

              painter: BackgroundPainter(animation: _controler.view),
            )
          ),
          Positioned(
            top: 60,
            left: 20,
            child: Text(
              "Welcome",
              style: TextStyle(
                fontSize: 45,
                letterSpacing: 6,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          StreamBuilder<int>(
            stream: _streamControler.stream,
            initialData: 0,
            builder: (context, snapshot) {


              if(snapshot.data==0)
                {
                  return Positioned(
                    top: MediaQuery.of(context).size.height/2,
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height,
                    child: LoginWidget(_loginControler, _passwordControler, _controler,_streamControler),

                  );
                }
              else return Positioned(

                  top: MediaQuery.of(context).size.height/5,
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height,

                  child: RegisterWidget(_controler,_streamControler)
              );

            }
          )
        ],
      ),
    );
  }
}
