import 'package:aystentapp/apiHelpers/apiHandler.dart';
import 'package:aystentapp/dataWrapers/Drug.dart';
import 'package:aystentapp/dataWrapers/DrugTaken.dart';
import 'package:aystentapp/dataWrapers/UserRaport.dart';
import 'package:aystentapp/dataWrapers/userData.dart';
import 'package:aystentapp/decorations/Colors.dart';
import 'package:aystentapp/decorations/textStyles.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class UserRaportScreen extends StatefulWidget {

  Drug selectedDrug;// = new Drug(name: 'Apap');
  TextEditingController _descControler = new TextEditingController();
  TextEditingController _amountControler = new TextEditingController();

  @override
  _UserRaportScreenState createState() => _UserRaportScreenState();
}

class _UserRaportScreenState extends State<UserRaportScreen> {

  UserData user;
  ApiHandler apiHandler = new ApiHandler();
  Future<List<Drug>> drugsFuture;

  void getAllDrugs() async{
    drugsFuture = apiHandler.getAllDrugs(user.token);
  }

  @override
  Widget build(BuildContext context) {

    if(user == null)
      {
        user = Provider.of<UserData>(context);
      }
    if(drugsFuture == null)
      {
        getAllDrugs();
      }
    return Scaffold(

      appBar: AppBar(
        title: Text("Create Your raport", style: slvappTextStyle,),
        backgroundColor: greenBlueColor,
      ),
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 20),
        child: Column(


          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Row(
              children: [
                Text("Drug taken", style: addRaportTextStyle,),
                Spacer(),
                FutureBuilder(
                  future: drugsFuture,
                  builder: (BuildContext context, AsyncSnapshot snapshot){

//                    print("User raport:conn state ${snapshot.connectionState.toString()}");
                    if(snapshot.connectionState == ConnectionState.done || snapshot.hasData)
                    {
//                    print("User raport: Has data ${snapshot.hasData}");
                    //print("doctor size: ${widget.doctors.length}");

                    if(widget.selectedDrug == null)
                      {
                        widget.selectedDrug = snapshot.data[0];
                      }
                    return DropdownButton(

                      value: widget.selectedDrug,
                        items: snapshot.data.map<DropdownMenuItem<Drug>>(
                            (val){
//                              print("Dropdown drug ${val.name}");
                              return DropdownMenuItem<Drug>(value: val, child: Text(val.name),);
                            }
                        ).toList(),
                      onChanged: (val){
                        setState(() {
                          widget.selectedDrug = val;
                        });
                      },
                    );
                    }
                    else{
//                    print("User raport: Hasn;t  data ${snapshot.hasData}");
                    return Center(child: Text(snapshot.hasData.toString()));
                    }
                  },
                )

              ],
            ),
            SizedBox(height: 10,),
            TextFormField(
              controller: widget._amountControler,
              keyboardType: TextInputType.number,
              maxLines: null,
              decoration: InputDecoration(
                labelText: "Amount",
                fillColor: greenBlueColor,
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(25.0),
                  borderSide: BorderSide(
                    color: greenBlueColor,
                  ),
                ),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(25.0),
                  borderSide: BorderSide(
                    color: greenBlueColor,
                    width: 2.0,
                  ),
                ),
              ),
            ),
            SizedBox(height: 10,),
            TextFormField(
              controller: widget._descControler,
              keyboardType: TextInputType.text,
              maxLines: 10,
              decoration: InputDecoration(
                labelText: "Description",
                fillColor: greenBlueColor,
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(25.0),
                  borderSide: BorderSide(
                    color: greenBlueColor,
                  ),
                ),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(25.0),
                  borderSide: BorderSide(
                    color: greenBlueColor,
                    width: 2.0,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),

      floatingActionButton: FloatingActionButton(
        backgroundColor: greenBlueColor,
        //isExtended: true,
        child: Icon(Icons.add, color: orangeColor,size: 30,),
        onPressed: () async
        {
          print("add pressed");
          UserRaport raport = new UserRaport(login: user.username,description: widget._descControler.text,drugTaken: DrugTaken(amount: int.parse(widget._amountControler.text),drug: widget.selectedDrug));
          bool result = await apiHandler.addUserReport(raport, user.token);
          if(result)
            {
              ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("User raport added succesfully")));
            }
          else{
            ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("User raport added failed")));
          }
        },
      ),
    );
  }
}
