import 'package:aystentapp/dataWrapers/userData.dart';
import 'package:aystentapp/decorations/Colors.dart';
import 'package:aystentapp/screens/settingsScreen.dart';
import 'package:aystentapp/screens/yourPatientsScreen.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class DoctorHome extends StatefulWidget {

  UserData userData;
  int currentScreenIndex = 0;
  DoctorHome(this.userData);
  @override
  _DoctorHomeState createState() => _DoctorHomeState();
}

class _DoctorHomeState extends State<DoctorHome> {
  @override
  Widget build(BuildContext context) {
    return  Scaffold(

      body: ChangeNotifierProvider<UserData>(

        create: (context){

          return  widget.userData;
        },
        child: IndexedStack(
          index: widget.currentScreenIndex,

          children: [
            YourPatientScreen(),
            SettingsScreen()
          ],
        ),
      ),


      bottomNavigationBar: CurvedNavigationBar(
        buttonBackgroundColor: Colors.blueGrey,
        backgroundColor: Colors.white,
        color: greenBlueColor,

        onTap: (idx){
          setState(() {
            widget.currentScreenIndex = idx;
          });
        },

        items: [
          Column(
            children: [
              Icon(Icons.home, size: 40, color: orangeColor,),
              //Text("Home")
            ],
          ),

          Column(
            children: [
              Icon(Icons.settings, size: 40, color: orangeColor,),
              //Text("Home")
            ],
          ),


        ],
      ),
    );
  }
}
