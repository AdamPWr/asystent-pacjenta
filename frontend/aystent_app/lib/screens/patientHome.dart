import 'package:aystentapp/dataWrapers/userData.dart';
import 'package:aystentapp/decorations/Colors.dart';
import 'package:aystentapp/screens/settingsScreen.dart';
import 'package:aystentapp/screens/yourDoctorsScreens.dart';
import 'package:flutter/material.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:provider/provider.dart';

import 'userRaportScreen.dart';

class PatientHome extends StatefulWidget {

  UserData userData; // needed to initialize userdata to provider
  int currentScreenIndex = 0;

  PatientHome(this.userData);
  @override
  _PatientHomeState createState() => _PatientHomeState();
}

class _PatientHomeState extends State<PatientHome> {
  @override
  Widget build(BuildContext context) {
    return  Scaffold(

      body: ChangeNotifierProvider<UserData>(

        create: (context){

          return  widget.userData;
        },
        child: IndexedStack(
          index: widget.currentScreenIndex,

          children: [
            YourDoctorsScreen(),
            UserRaportScreen(),
            SettingsScreen()
          ],
        ),
      ),


      bottomNavigationBar: CurvedNavigationBar(
        buttonBackgroundColor: Colors.blueGrey,
        backgroundColor: Colors.white,
        color: greenBlueColor,
        onTap: (idx){
          setState(() {
            widget.currentScreenIndex = idx;
          });
        },

        items: [
          Column(
            children: [
              Icon(Icons.home, size: 40, color: orangeColor,),
              //Text("Home")
            ],
          ),
          Column(
            children: [
              Icon(Icons.add_comment_rounded, size: 40, color: orangeColor,),
              //Text("Home")
            ],
          ),
          Column(
            children: [
              Icon(Icons.settings, size: 40, color: orangeColor,),
              //Text("Home")
            ],
          ),


        ],
      ),
    );
  }
}
