import 'package:aystentapp/apiHelpers/apiHandler.dart';
import 'package:aystentapp/common/userRaportTile.dart';
import 'package:aystentapp/dataWrapers/UserRaport.dart';
import 'package:aystentapp/dataWrapers/userData.dart';
import 'package:aystentapp/decorations/Colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:provider/provider.dart';


class UserRaportsScreen extends StatefulWidget {

  UserRaportsScreen({this.prevCtx});

  BuildContext prevCtx;
  ApiHandler apiHandler  =new ApiHandler();

  @override
  _UserRaportsScreenState createState() => _UserRaportsScreenState();
}

class _UserRaportsScreenState extends State<UserRaportsScreen> {

  UserData userData;

  List<UserRaport> reports;
  Future<List<UserRaport>> allReportsFuture;

  void getAllReports() async{
    allReportsFuture =  widget.apiHandler.getUserRaports(userData);
    await allReportsFuture.then((value)  {

      print("reports: ${value.length}");
      reports = value;
    });
  }


  @override
  Widget build(BuildContext context) {

    if(userData == null)
      {
        userData = Provider.of<UserData>(widget.prevCtx);
      }
    if(reports == null)
      {
        getAllReports();
      }
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: greenBlueColor,
        title: Text("${userData.username} raports", style: TextStyle(color: orangeColor),),
      ),

      body: FutureBuilder(
        
        future: allReportsFuture,
        builder: (BuildContext context, AsyncSnapshot snapshot){
          if(snapshot.connectionState == ConnectionState.done || snapshot.hasData)
            {
              return ListView.builder(
                itemCount: reports.length,
                  itemBuilder: (BuildContext context,int i){
                  print("IDX ; $i");
                  return UserRaportTile(amount: reports[i].drugTaken.amount,drug: reports[i].drugTaken.drug.name,date: reports[i].date,description: reports[i].description);
                  }
              );
            }
          else
            {
              return const  SpinKitRotatingCircle(
                color: Colors.blueGrey,
                size: 50.0,
              );
            }
        },
      )
    );
  }
}
