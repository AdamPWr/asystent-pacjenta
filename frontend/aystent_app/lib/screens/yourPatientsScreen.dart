import 'package:aystentapp/apiHelpers/apiHandler.dart';
import 'package:aystentapp/common/PatientsListWidget.dart';
import 'package:aystentapp/dataWrapers/userData.dart';
import 'package:aystentapp/decorations/Colors.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class YourPatientScreen extends StatefulWidget {

  UserData userData;
  ApiHandler apiHandler = new ApiHandler();

  @override
  _YourPatientScreenState createState() => _YourPatientScreenState();
}

class _YourPatientScreenState extends State<YourPatientScreen> {

  List<UserData> patients;
  Future<List<UserData>> yourPatientsFuture;


  void getAllPatients() async{
    yourPatientsFuture =  widget.apiHandler.myPatients(widget.userData);
    await yourPatientsFuture.then((value)  {

      print("Patients: ${value.length}");
      patients = value;
    });
  }

  @override
  Widget build(BuildContext context) {

    if(widget.userData == null)
      {
        widget.userData = Provider.of<UserData>(context);
      }
    if(patients == null)
    {
      getAllPatients();
    }
    return Scaffold(
      body: FutureBuilder(
          future: yourPatientsFuture,
          builder: (BuildContext context, AsyncSnapshot snapshot)
          {
            if(snapshot.connectionState == ConnectionState.done || snapshot.hasData)
              {
                return PatientsListWidget(userData: widget.userData,patients: patients,);
              }
            else
              {
                return const  SpinKitRotatingCircle(
                  color: Colors.blueGrey,
                  size: 50.0,
                );
              }

          }

      ),

      floatingActionButton: FloatingActionButton(
        backgroundColor: greenBlueColor,
        //isExtended: true,
        child: Icon(Icons.add, color: orangeColor,size: 30,),

        onPressed: ()
        {
          Navigator.pushNamed(context, '/addPatient',arguments: context);
        },

      ),
    );
  }
}
