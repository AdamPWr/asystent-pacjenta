import 'package:aystentapp/apiHelpers/apiHandler.dart';
import 'package:aystentapp/dataWrapers/userData.dart';
import 'package:aystentapp/decorations/Colors.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SettingsScreen extends StatefulWidget {
  @override
  _SettingsScreenState createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {

  ApiHandler apiHandler = new ApiHandler();
  UserData userData;

  @override
  Widget build(BuildContext context) {

    if(userData == null)
    {
      userData = Provider.of<UserData>(context);
    }

    return Center(
      child: Column(

        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,

        children: [
          ElevatedButton(
            onPressed: (){
              Navigator.popAndPushNamed(context, '/login');
            },
            style: ElevatedButton.styleFrom(
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
                primary: greenBlueColor),
            child: Container(
                width: 200,
                height: 160,
                alignment: Alignment.center,
                //decoration: BoxDecoration(shape: BoxShape.circle, color: greenBlueColor),
                child: Text(
                  "Log out",
                  style: TextStyle(
                      color: orangeColor,
                      letterSpacing: 5,
                      fontSize: 20
                  ),
                )
            ),
          ),
          SizedBox(height: 10,),
          ElevatedButton(
            onPressed: () async {
              bool result = await apiHandler.deleteUser(userData.username, userData.token);

              if(result)
                {
                  Navigator.popAndPushNamed(context, '/login');
                }
              else{
                ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Deleting account failed")));
              }
            },
            style: ElevatedButton.styleFrom(
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
                primary: greenBlueColor),
            child: Container(
                width: 200,
                height: 160,
                alignment: Alignment.center,
                //decoration: BoxDecoration(shape: BoxShape.circle, color: greenBlueColor),
                child: Text(
                  "Delete Account",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: orangeColor,
                      letterSpacing: 5,
                      fontSize: 20,
                  ),
                )
            ),
          ),
        ],

      ),
    );
  }
}
