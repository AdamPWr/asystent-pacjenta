import 'package:aystentapp/decorations/Colors.dart';
import 'package:flutter/material.dart';

class SendSmsScreen extends StatefulWidget {

  SendSmsScreen({this.number});


  String number;


  @override
  _SendSmsScreenState createState() => _SendSmsScreenState();
}

class _SendSmsScreenState extends State<SendSmsScreen> {

  TextEditingController _controler = TextEditingController();

  @override
  Widget build(BuildContext context) {

    var height = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: greenBlueColor,
        title: Text("Send message", style: TextStyle(color: orangeColor),),
      ),
      body: Column(

        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          TextFormField(
            controller: _controler,
            keyboardType: TextInputType.multiline,
            maxLines: null,
            decoration: InputDecoration(
                labelText: "Enter text message",
                focusColor: greenBlueColor,
                contentPadding: EdgeInsets.symmetric(vertical: height * 0.20, horizontal: 10),
                fillColor: Colors.grey,
                border: OutlineInputBorder(borderRadius: BorderRadius.circular(10.0))
            ),
          ),
      SizedBox(height: 10,),
      ElevatedButton(
        onPressed: (){
          Navigator.pop(context);
        },
        style: ElevatedButton.styleFrom(
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(640.0)),
            primary: greenBlueColor),
        child: Container(
            width: 100,
            height: 80,
            alignment: Alignment.center,
            //decoration: BoxDecoration(shape: BoxShape.circle, color: greenBlueColor),
            child: Text(
              "Send",
              style: TextStyle(
                  color: orangeColor,
                  letterSpacing: 5,
                  fontSize: 20
              ),
            )
        ),
      )
        ],
      ),
    );
  }
}
