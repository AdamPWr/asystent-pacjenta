import 'package:aystentapp/apiHelpers/apiHandler.dart';
import 'package:aystentapp/dataWrapers/userData.dart';
import 'package:aystentapp/decorations/Colors.dart';
import 'package:aystentapp/decorations/textStyles.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class AddPatientscreen extends StatefulWidget {

  AddPatientscreen({this.precCtx});
  TextEditingController loginControler = new TextEditingController();

  BuildContext precCtx;
  @override
  _AddPatientscreenState createState() => _AddPatientscreenState();
}

class _AddPatientscreenState extends State<AddPatientscreen> {

  ApiHandler apiHandler = new ApiHandler();
  UserData user;

  @override
  Widget build(BuildContext context) {

    if(user == null)
      {
        user = Provider.of<UserData>(widget.precCtx);
      }
    return Scaffold(

      appBar: AppBar(
        title: Text("Add patient", style: slvappTextStyle,),
        backgroundColor: greenBlueColor,
        centerTitle: true,
      ),

      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            Text(
              'If You want to became supervisor of new person, just enter his login in the form below.',
              maxLines: 8,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                  color: Colors.grey[800],
                  fontWeight: FontWeight.w900,
                  fontStyle: FontStyle.italic,
                  fontFamily: 'Open Sans',
                  fontSize: 30),
            ),
            SizedBox(height: 10,),
            TextFormField(
              //obscureText: true,
              controller: widget.loginControler,
              //keyboardType: TextInputType.phone,
              decoration: InputDecoration(
                labelText: "Login",
                fillColor: Colors.white,
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(25.0),
                  borderSide: BorderSide(
                    color: Colors.blueAccent,
                  ),
                ),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(25.0),
                  borderSide: BorderSide(
                    color: Colors.blue,
                    width: 2.0,
                  ),
                ),
              ),
            ),
            SizedBox(height: 20,),
            ElevatedButton(
              onPressed: ()async {
                bool result = await apiHandler.addPatientBinding(user, widget.loginControler.text);
                Navigator.pop(context);

              },
              style: ElevatedButton.styleFrom(
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(640.0)),
                  primary: greenBlueColor),
              child: Container(
                  width: 100,
                  height: 80,
                  alignment: Alignment.center,
                  //decoration: BoxDecoration(shape: BoxShape.circle, color: greenBlueColor),
                  child: Text(
                    "Submit",
                    style: TextStyle(
                        color: orangeColor,
                        letterSpacing: 5,
                        fontSize: 20
                    ),
                  )
              ),
            )
          ],
        ),
      ));
  }
}
