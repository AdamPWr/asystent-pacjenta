import 'package:aystentapp/apiHelpers/apiHandler.dart';
import 'package:aystentapp/common/doctorListWidget.dart';
import 'package:aystentapp/dataWrapers/userData.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:provider/provider.dart';
//import 'dart:developer' as logger;

class YourDoctorsScreen extends StatefulWidget {
  YourDoctorsScreen();

  @override
  _YourDoctorsScreenState createState() => _YourDoctorsScreenState();

  UserData userData;
  ApiHandler apiHandler = new ApiHandler();

}

class _YourDoctorsScreenState extends State<YourDoctorsScreen> {

  List<UserData> doctors;
  Future<List<UserData>> allDoctorsFuture;

  void getAllDoctors() async{
  allDoctorsFuture =  widget.apiHandler.allDoctors(widget.userData);
    await allDoctorsFuture.then((value)  {

      print("doc: ${value.length}");
      doctors = value;
    });
  }

  @override
  void initState()  {
    // TODO: implement initState
    super.initState();
  }



  @override
  Widget build(BuildContext context) {

    widget.userData = Provider.of<UserData>(context);
    if(doctors == null)
      {
        getAllDoctors();
      }
//    print("YourDoctorsScreen name: ${widget.userData.name}");
//    print("YourDoctorsScreen token: ${widget.userData.token}");
//    print("YourDoctorsScreen doc len: ${doctors?.length}");
    var size = MediaQuery.of(context).size;

    return FutureBuilder(

      future: allDoctorsFuture,
      builder: (BuildContext context, AsyncSnapshot snapshot){
        //logger.log("elo");
        print("conn state ${snapshot.connectionState.toString()}");
        if(snapshot.connectionState == ConnectionState.done || snapshot.hasData)
          {
            print("Has data ${snapshot.hasData}");
            //print("doctor size: ${widget.doctors.length}");
            return DocListWidget(userData: widget.userData, doctors: doctors,);
          }
        else{
          print("Hasn;t  data ${snapshot.hasData}");
          return const  SpinKitRotatingCircle(
            color: Colors.blueGrey,
            size: 50.0,
          );
        }
      },

    );


  }
}




