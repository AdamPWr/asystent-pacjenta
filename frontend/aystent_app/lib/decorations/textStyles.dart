import 'package:aystentapp/decorations/Colors.dart';
import 'package:flutter/material.dart';

TextStyle slvappTextStyle  = TextStyle(
  color: orangeColor,
  fontSize: 20
);

TextStyle patientTtyleileTextStyle  = TextStyle(
    color: Colors.blueGrey,
    fontSize: 15
);

TextStyle addRaportTextStyle  = TextStyle(
    color: greenBlueColor,
    fontSize: 18
);
